import React from "react";
import Header from "../components/header";
import Content from "../components/content";
import Footer from "../components/footer";

// markup
const IndexPage = () => {
  return <>
    <Header/>
    <Content/>
    <Footer/>
    </>
};

export default IndexPage;
