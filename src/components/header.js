import React from "react";

import 'bootstrap/dist/css/bootstrap.css';
import '/src/styles/styles.scss'

import {Container, Row, Col, Button} from 'reactstrap';

const Header = () => {

    return <Container className="header" fluid>
        <img
            data-sal-delay="100"
            data-sal="slide-down"
            data-sal-duration="600"
            data-sal-easing="ease"
            src="images/logo.svg" alt="logo" className="header__logo"/>
        <a data-sal="slide-up"
           data-sal-delay="100"
           data-sal-duration="400"
           data-sal-easing="ease"
           className="header__button" href="#content">ZOBACZ OFERTĘ</a>
    </Container>
};

export default Header;
