import React, {useEffect, useState} from "react";

import 'bootstrap/dist/css/bootstrap.css';
import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';
import '/src/styles/styles.scss'
import {Container, Row, Col, Button} from 'reactstrap';
import {CopyToClipboard} from 'react-copy-to-clipboard';


const TitleIconStyle = {
    top: '-.25rem',
    left: '-2.25rem',
    width: '7rem',
    height: '7rem',
}

const TitleIconStyleMini = {
    top: '-.75rem',
    left: '-.75rem',
    width: '4rem',
    height: '4rem',
}
const TitleStyle = {
    padding: 0,
    marginLeft: '120px'
}

const TitleIconStyleMiniMobile = {
    top: '0rem',
    width: '2.5rem',
    height: '2.5rem',
    left: '-.12rem',
}
const Content = () => {


    const [width, setWitdh] = useState(null)
    useEffect(() => {
        console.log(window.innerWidth)
        setWitdh(window.innerWidth)
    }, [])


    const isMobile = width < 992

    return <Container fluid className="content" id="content">
        <VerticalTimeline
            className="content__verticaltimeline"
            layout='1-column-left'>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img styles={{background: '#202020'}} src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMini : TitleIconStyle}
            >
                <h1 className="content__verticaltimeline__element__title">Oferta</h1>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <p className="content__verticaltimeline__element__paragraph">Wszystko co wiąże się z szeroko pojętą
                    <strong> kreacją twórczą</strong>. Pomożemy Ci stworzyć wszystko, czego potrzebujesz, aby wyrazić
                    siebie lub swój
                    biznes w sieci.</p>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
            >
                <p className="content__verticaltimeline__element__paragraph">Zajmujemy się nie tylko stroną wizualną,
                    ale również pracą redakcyjną na
                    artystycznym poziomie.</p>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <Row className="content__row">
                    <Col lg="6" className="content__col">
                        <div className="content__col__text">
                            <h1 className="content__col__text__header">KONTENT WIZUALNY/ GRAFICZNY</h1>
                            <p className="content__col__text__paragraph">
                                <strong>Branding Twojej firmy,</strong><br/> zaczynając od logo, wizytówki czy ulotki a
                                kończąc na
                                billboardach.


                            </p>
                            <p className="content__col__text__paragraph">

                                <strong> Strona internetowa od podstaw.</strong><br/> Wliczając napisanie treści,
                                wykonanie potrzebnych grafik
                                oraz zdjęć.

                            </p>
                            <p className="content__col__text__paragraph">
                                <strong> Linie ubrań </strong>od wzoru i szablonu do wizualizacji.


                            </p>
                            <p className="content__col__text__paragraph">

                                <strong> Spójna identyfikacja wizualna </strong><br/>kanału na Youtube bądź koncie w
                                mediach
                                społeczniościowych. Zaczynając od zdjęć w tle a kończąc na wielofunkcyjnych animacjach
                                do Twoich filmów.

                            </p>
                            <p className="content__col__text__paragraph">
                                <strong>Prowadzenie social mediów </strong>Twojej firmy.
                                Ciekawe i absorbujące
                                posty, przyciągające uwagę potencjalnych odbiorców.</p>
                            <p className="content__col__text__paragraph">
                                <strong> Twój ebook.</strong><br/> Od samego początku do samego końca. Zdjęcia
                                produktowe, projekt, montaż aż
                                do gotowego produktu, który możesz sprzedawać na swojej stronie.


                            </p>
                        </div>
                    </Col>
                    <Col lg="6" className="content__col">
                        <div className="content__col__text">
                            <h1 className="content__col__text__header">KONTENT REDAKCYJNY</h1>
                            <p className="content__col__text__paragraph">Zajmujemy się również wszelaką redakcją tekstów
                                słownych, w której skład wchodzi opisywanie Twoich przeżyć czy przekazu do świata w
                                <strong> sposób pełny artyzmu i emocji.</strong> Nie jest istotne czy to treść plakatu,
                                strony
                                internetowej, artykułu, referatu czy <strong> historii Twojego życia.</strong></p>
                            <p
                                className="content__col__text__paragraph"> Dla jednego z nas pisanie jest sposobem na
                                głębokie wyrażanie siebie i poprzez teksty spod naszego pióra wyrazimy nimi Ciebie.
                                Pomożemy ci słowami <strong> wyrazić to co jest o Tobie. </strong></p>

                            <p
                                className="content__col__text__paragraph"> Nie ma takiej rzeczy, której nie bylibyśmy w
                                stanie wykonać. Jesteśmy tutaj, aby <strong> tworzyć </strong> rzeczy, które wychodzą po
                                za
                                wszelkie
                                schematy.</p>

                        </div>
                        <a href="#contact" className="content__col__text__button">
                            SKONTAKTUJ SIĘ Z NAMI
                        </a>
                    </Col>
                </Row>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img styles={{background: '#202020'}} src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMini : TitleIconStyle}
            >
                <h1 className="content__verticaltimeline__element__title">O firmie</h1>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <h1 className="content__verticaltimeline__element__title p-0" style={{fontSize: '3rem'}}>COMTRU TO
                    AGENCJA KREATYWNA</h1>
                <p className="content__verticaltimeline__element__paragraph">skupiająca się na tworzeniu przekazu treści
                    i wizualizacji swoich klientów. Cechuje nas bardzo duża śmiałość w tworzeniu materiałów
                    nietuzinkowych, odbiegających od marketingowych i społecznych szablonów oraz konwenansów. </p>
                <p className="content__verticaltimeline__element__paragraph">Jesteśmy zainteresowani współpracą z
                    osobami prywatnymi jak i firmami, które chcą łamać ustalone schematy swojej branży. Nieszablonowość,
                    nastawienie na odczuwanie i przekazywanie emocji, prawdziwość, autentyczność. Odwaga w wyrażaniu
                    siebie, takim jakim się jest, a nie jakim wypada być w świecie poważnego biznesu. </p>
                <p className="content__verticaltimeline__element__paragraph">Głęboko wierzymy, że suwerenne kroczenie za
                    sobą i swoją Prawdą jest kluczem do radosnego i spełnionego życia oraz biznesu. </p>
                <p className="content__verticaltimeline__element__paragraph">Nawet ten opis naszej firmy jest niezgodny
                    z tym, jak powinno się reklamować, docierać do klienta i przedstawiać siebie w sieci.</p>
      <p className="content__verticaltimeline__element__paragraph">Nas to jednak nie obchodzi, ponieważ nie jesteśmy tutaj, aby się podobać, ale aby pomagać innym wyrażać siebie.</p>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img styles={{background: '#202020'}} src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMini : TitleIconStyle}
            >
                <h1 className="content__verticaltimeline__element__title">O nas</h1>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <h1 className="content__verticaltimeline__element__title p-0" style={{fontSize: '3rem'}}>COMTRU</h1>
                <p className="content__verticaltimeline__element__paragraph">Comtru to marka stworzona
                    przez <strong>rodzeństwo. </strong>
                    Młodszy brat i starsza siostra w procesie Życia, które się
                    wydarza <strong>Teraz.</strong> Wychodzimy z dwóch różnych
                    pokoleń i środowisk. Jesteśmy swoimi skrajnymi przeciwnościami, dlatego tak wspaniale się
                    uzupełniamy.</p>
                <p className="content__verticaltimeline__element__paragraph">Cechuje nas obojga duża niezależność,
                    autonomiczność, artyzm oraz nietuzinkowość. Nie spotykamy się z Tobą w szpilkach i pod krawatem.
                    Cenimy sobie wygodę, luz oraz bycie <strong>autentycznym.</strong> Eff ujrzysz w kwiecistych
                    sukniach ciągnących się
                    po ziemi, a Mata w luźnym stylu. Ciebie również pragniemy ujrzeć w swojej Prawdzie, ponad
                    społecznymi dogmatami.</p>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <Row className="content__row">
                    <Col lg="4">
                        <img className="w-100 h-100 content__image" src="images/yeti.jpg"/>
                    </Col>
                    <Col lg="8" style={{marginTop: '1rem'}}>
                        <h1 className="content__verticaltimeline__element__title"
                            style={{fontSize: '3rem'}}>Eff Kozłowska</h1>
                        <p className="font-monster" style={{fontSize: '1.5rem'}}>
                            Żeńska część Comtru. Odpowiada w większości za czucie, emocjonalność, uważność, przepływ.
                            Założycielka projektu O zapachu słońca. Autorka książki „Chłonę wszystko”. W firmie
                            odpowiedzialna za wszystkie treści pisane, ponieważ pisanie to jej największa życiowa pasja.
                            Portrecistka od trzynastego roku życia. Bawi się światłem i koloruje Twoje zdjęcia. W życiu
                            najbardziej ceni sobie wyrażanie samej siebie i w Comtru tworzy treści, które w tym samym
                            wspierają klienta i jego osobistą Prawdę.
                        </p>
                    </Col>
                </Row>

                <Row className="content__row mt-4">

                    <Col lg="8">
                        <h1 className="content__verticaltimeline__element__title p-0"
                            style={{fontSize: '3rem'}}>Mateusz Wichary</h1>
                        <p className="font-monster" style={{fontSize: '1.5rem'}}>
                            Męska część Comtru. Odpowiada za całą techniczną i warsztatową część kreacji. Umysł ścisły o
                            nieszablonowym koncepcie. Zajmuje się wszelkimi sprawami ilustracyjnymi. Po ukończeniu
                            szkoły wyprowadził się z domu i zajął się profesjonalną pracą graficzną . Prywatnie
                            zafascynowany nowoczesną kinematografią, światem dwuwymiarowej animacji oraz kulturą
                            japońską.
                        </p>
                    </Col>
                    <Col lg="4">
                        <img className="w-100 h-100 content__image" src="images/mat.jpg"/>
                    </Col>
                </Row>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img styles={{background: '#202020'}} src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMini : TitleIconStyle}
            >
                <h1 id="contact" className="content__verticaltimeline__element__title">Kontakt</h1>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <p className="content__verticaltimeline__element__paragraph">Napisz nam czego potrzebujesz. Opisz to
                    tak, jak <strong>czujesz.</strong></p>
                <p className="content__verticaltimeline__element__paragraph">My przygotujemy dla Ciebie wycenę.
                </p>

                <CopyToClipboard text="kontakt@comtru.art">
                    <div className="content__copy font-monster">kontakt@comtru.art <img
                        className="content__copy__icon" src="icons/copy.svg"/></div>
                </CopyToClipboard>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <h1 className="content__verticaltimeline__element__title p-0" style={{fontSize: '3rem'}}>Dane</h1>
                <p className="content__verticaltimeline__element__paragraph">O zapachu słońca sp z o. o.
                </p>
                <CopyToClipboard text="NIP 547 222 3483">
                    <div className="content__copy font-monster"> NIP 547 222 3483<img
                        className="content__copy__icon" src="icons/copy.svg"/></div>
                </CopyToClipboard>
                <p className="content__verticaltimeline__element__paragraph"> ING Bank Śląski:
                </p>
                <CopyToClipboard text="24 1050 1070 1000 0090 8118 7792">
                    <div className="content__copy font-monster">24 1050 1070 1000 0090 8118 7792 <img
                        className="content__copy__icon" src="icons/copy.svg"/></div>
                </CopyToClipboard>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                className="vertical-timeline-element--work content__verticaltimeline__element"
                contentStyle={TitleStyle}
                icon={<img src="images/logo-wheel.svg"/>}
                iconStyle={isMobile ? TitleIconStyleMiniMobile : TitleIconStyleMini}
            >
                <h1 className="content__verticaltimeline__element__title p-0" style={{fontSize: '3rem'}}>Social
                    Media</h1>

                <a target="_blank" href="https://www.facebook.com/Comtruart-111539867975956/"
                   className="content__link font-monster">
                    <img src="icons/fb.png"/> Facebook
                </a>
                <br/>
                <a target="_blank" href="https://www.instagram.com/comtru.art/" className="content__link font-monster">
                    <img src="icons/insta.png"/> Instagram
                </a>
            </VerticalTimelineElement>
        </VerticalTimeline>
    </Container>
}


export default Content
